<?php

use App\Http\Controllers\GameController;
use App\Models\Game;
use App\Services\ComputerIntelligenceService;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GameController::class, 'index'])->name('game.index');
Route::match(['get', 'post'],'/connect', [GameController::class, 'connectGame'])->name('game.connect');
Route::get('/select-mode', [GameController::class, 'selectMode'])->name('game.select-mode');
Route::get('/difficulty-level', [GameController::class, 'difficultyLevel'])->name('game.difficulty-level');
Route::post('/game/store', [GameController::class, 'store'])->name('game.store');
Route::get('/game/create', [GameController::class, 'create'])->name('game.create');
Route::get('/game/{gameId}', [GameController::class, 'show'])->name('game.start');
