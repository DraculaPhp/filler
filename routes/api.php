<?php

use App\Http\Controllers\API\GameController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/game', [GameController::class, 'createGame']);
Route::get('/game/{gameId}', [GameController::class, 'getGame']);
Route::put('/game/{gameId}', [GameController::class, 'makeStep']);
Route::put('/game/single/{gameId}', [GameController::class, 'makeComputerStep']);
