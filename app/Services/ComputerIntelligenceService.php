<?php

namespace App\Services;

use App\Models\Game;
use Exception;

/**
 * @property Game $game
 */
class ComputerIntelligenceService
{
    /**
     * @var Game
     */
    public $game;

    /**
     * @param Game $game
     * @param $uuid
     */
    public function __construct(
        Game $game,
        $uuid = null
    ) {
        if ($uuid) {
            $this->game = $game->getGame($uuid);
        } else {
            $this->game = $game;
        }
    }

    /**
     * @param int $number
     * @param int $width
     *
     * @return array
     */
    public function getNeighboringCases(int $number, int $width): array
    {
        $cases = [];
        if (($number - ($width - 1) / 2) % $width !== 0) {
            $cases[] = $number + ($width + 1) / 2;
            if ($number - ($width - 1) / 2 >= 0) {
                $cases[] = $number - ($width - 1) / 2;
            }
        }
        if ($number % $width !== 0) {
            $cases[] = $number + ($width - 1) / 2;
            if ($number - ($width + 1) / 2 >= 0) {
                $cases[] = $number - ($width + 1) / 2;
            }
        }
        return $cases;
    }

    /**
     * @param int   $number
     * @param array $fieldData
     *
     * @return array
     */
    public function getNeighboringCells(int $number, array $fieldData): array
    {
        $cases = $this->getNeighboringCases($number, $fieldData['width']);
        $cells = [];
        foreach ($cases as $case) {
            if (isset($fieldData['cells'][$case])) {
                $cells[$case] = $fieldData['cells'][$case];
            }
        }
        return $cells;
    }

    /**
     * @param array $cell
     * @param int   $number
     * @param int   $playerId
     * @param array $fieldData
     *
     * @return array
     */
    public function assignCellsToPlayerRecursive(array $cell, int $number, int $playerId, array $fieldData): array
    {
        ini_set('xdebug.max_nesting_level', 2000);

        $neighboringCells = $this->getNeighboringCells($number, $fieldData);
        $neighboringCellsNextColor = [];
        foreach ($neighboringCells as $key => $neighboringCell) {
            if ($neighboringCell['color'] === $cell['color']) {
                $neighboringCellsNextColor[$key] = $neighboringCell;
            }
        }

        if (empty($neighboringCellsNextColor)) {
            return $fieldData;
        }

        foreach ($neighboringCellsNextColor as $key => $neighboringCell) {
            if ($neighboringCell['playerId'] !== $playerId) {
                $fieldData['cells'][$key]['playerId'] = $playerId;
                $fieldData = $this->assignCellsToPlayerRecursive($fieldData['cells'][$key], $key, $playerId, $fieldData);
            }
        }
        return $fieldData;
    }

    /**
     * @param int   $playerId
     * @param array $fieldData
     *
     * @return array
     */
    public function getCellsByPlayerId(int $playerId, array $fieldData): array
    {
        $cells = [];
        foreach ($fieldData['cells'] as $key => $cell) {
           if ($cell['playerId'] === $playerId) {
               $cells[$key] = $cell;
           }
        }
        return $cells;
    }

    /**
     * @param $cells
     * @param $nextColor
     * @param $fieldData
     */
    public function changeCellsColor(&$cells, $nextColor, &$fieldData): void
    {
        foreach ($cells as $key => $cell) {
            $fieldData['cells'][$key]['color'] = $nextColor;
            $cells[$key]['color'] = $nextColor;
        }
    }

    /**
     * @param int    $playerId
     * @param string $nextColor
     * @param array  $fieldData
     *
     * @return array
     */
    public function simulateStep(int $playerId, string $nextColor, array $fieldData): array
    {
        $currentUserCells = $this->getCellsByPlayerId($playerId, $fieldData);
        $this->changeCellsColor($currentUserCells, $nextColor, $fieldData);

        foreach ($currentUserCells as $number => $cell) {
            $fieldData = $this->assignCellsToPlayerRecursive($cell, $number, $playerId, $fieldData);
        }

        return $fieldData;
    }

    /**
     * @param int  $playerId
     * @param Game $game
     *
     * @return string
     */
    public function selectBestColor(int $playerId, Game $game): string
    {
        $availableColors = $game->getAvailableColors();

        $scores = [];
        $fieldData = $game->field->getFieldData();
        foreach ($availableColors as $color) {
            $data = $this->simulateStep($playerId, $color, $fieldData);
            $scores[$color] = count($this->getCellsByPlayerId($playerId, $data));
        }

        return array_keys($scores, max($scores))[0];
    }

    /**
     * @param int  $playerId
     * @param Game $game
     *
     * @return string
     */
    public function selectBestColorAdvanced(int $playerId, Game $game): string
    {
        $availableColors = $game->getAvailableColors();

        $scores = [];
        $fieldData = $game->field->getFieldData();
        $oldPlayerCells = $this->getCellsByPlayerId($playerId, $fieldData);

        $enemyId = 1;
        if ($playerId === 1) {
            $enemyId = 2;
        }

        foreach ($availableColors as $color) {
            $data = $this->simulateStep($playerId, $color, $fieldData);
            $enemyData = $this->simulateStep($enemyId, $color, $fieldData);

            $currentPlayerCells = $this->getCellsByPlayerId($playerId, $data);
            $capturedCells = array_filter($currentPlayerCells, static function($key) use ($oldPlayerCells) {
                return !array_key_exists($key, $oldPlayerCells);
            }, ARRAY_FILTER_USE_KEY);
            $capturedCellsScore = 0;
            foreach ($capturedCells as $key => $capturedCell) {
                $capturedCellsScore += $this->assignScoreToCell($key, $enemyData, $enemyId);
            }
            $scores[$color] = $capturedCellsScore;
        }

        return array_keys($scores, max($scores))[0];
    }

    /**
     * @param int   $number
     * @param array $enemyData
     * @param int   $enemyId
     *
     * @return int
     */
    public function assignScoreToCell(int $number, array $enemyData, int $enemyId): int
    {
        $score = 1;

        if (isset($enemyData['cells'][$number]) && $enemyData['cells'][$number]['playerId'] === $enemyId) {
            $score++;
        }

        return $score;
    }

    /**
     * @param Game $game
     *
     * @return string
     * @throws Exception
     */
    public function selectRandomColor(Game $game): string
    {
        $availableColors = $game->getAvailableColors();
        $availableColors = array_values($availableColors);
        $color = '';
        $index = random_int(0, 4);
        foreach ($availableColors as $key => $value) {
            if ($index === $key) {
                $color = $value;
            }
        }
        return $color;
    }

    /**
     * @param array $fieldData
     *
     * @return array
     */
    public function determineCenterZone(array $fieldData): array
    {
        $width = $fieldData['width'];
        $cells = $fieldData['cells'];
        $centerCells = [];

        if ((count($cells) - 1) % 2 === 0) {
            $number = (count($cells) - 1) / 2;
            $centerCells[$number] = $cells[$number];
            $centerCells = array_replace($this->getNeighboringCells($number, $fieldData), $centerCells);
        } else {
            $centerCells[count($cells) / 2] = $cells[count($cells) / 2];
            $centerCells[count($cells) / 2 - 1] = $cells[count($cells) / 2 - 1];
            $centerCells[count($cells) / 2 - ($width + 1) / 2] = $cells[count($cells) / 2 - ($width + 1) / 2];
            $centerCells[count($cells) / 2 + ($width - 1) / 2] = $cells[count($cells) / 2 + ($width - 1) / 2];
        }

        return $centerCells;
    }

    /**
     * @param array $fieldData
     *
     * @return int
     */
    public function getFirstPlayerStartCellNumber(array $fieldData): int
    {
        return count($fieldData['cells']) - ($fieldData['width'] + 1) / 2;
    }

    /**
     * @param array $fieldData
     * @return int
     */
    public function getSecondPlayerStartCellNumber(array $fieldData): int
    {
        return ($fieldData['width'] - 1) / 2;
    }

    /**
     * @param array $fieldData
     * @param int   $playerId
     * @param array $availableColors
     *
     * @return false|string
     */
    public function centerCaptureStrategy(array $fieldData, int $playerId, array $availableColors)
    {
        if ($playerId === 1) {
            $startNumber = $this->getFirstPlayerStartCellNumber($fieldData);
            $startCell = $fieldData['cells'][$startNumber];

            $capturedCell = $startCell;
            $number = $startNumber;
            while ($capturedCell['playerId'] === 1) {
                $capturedCell = $fieldData['cells'][$number - ($fieldData['width'] - 1) / 2];
                $number = $number - ($fieldData['width'] - 1) / 2;
            }
        } else {
            $startNumber = $this->getSecondPlayerStartCellNumber($fieldData);
            $startCell = $fieldData['cells'][$startNumber];

            $capturedCell = $startCell;
            $number = $startNumber;
            while ($capturedCell['playerId'] === 2) {
                $capturedCell = $fieldData['cells'][$number + ($fieldData['width'] - 1) / 2];
                $number = $number + ($fieldData['width'] - 1) / 2;
            }
        }

        if ($capturedCell['playerId'] !== 0 || !in_array($capturedCell['color'], $availableColors, true)) {
            return false;
        }

        return $capturedCell['color'];
    }

    /**
     * @param Game $game
     * @param int  $playerId
     *
     * @return string
     */
    public function adaptiveStrategy(Game $game, int $playerId): string
    {
        $fieldData = $game->field->getFieldData();
        $availableColors = $game->getAvailableColors();
        $centerCells = $this->determineCenterZone($fieldData);

        $alliesCaptured = array_filter($centerCells, static function($item) use ($playerId) {
            return $item['playerId'] === $playerId;
        });
        $enemiesCaptured = array_filter($centerCells, static function($item) use ($playerId) {
            return $item['playerId'] !== $playerId && $item['playerId'] !== 0;
        });

        if (empty($alliesCaptured) && count($enemiesCaptured) < 3) {
            $color = $this->centerCaptureStrategy($fieldData, $playerId, $availableColors);
            if ($color) {
                return $color;
            }
        }

        return $this->selectBestColorAdvanced($playerId, $game);
    }

    public function bestStrategy()
    {
        $strategy1 = 0;
        $strategy2 = 0;

        for ($i = 0; $i < 1; $i++) {
            $game = $this->game->createGame(21,21);
            $uuid = $game->uuid;
            if ($game) {
                while ($game->winner === 0) {
                    $nextColor = $this->selectBestColor(1, $game);
                    $game->field->makeStep(1, $nextColor);

                    $adaptiveColor = $this->selectBestColorAdvanced(2, $game);
                    $game->field->makeStep(2, $adaptiveColor);

                    $game = $this->game->getGame($uuid);
                }
            }
            if ($game->winner === 1) {
                $strategy1++;
            } elseif ($game->winner === 2) {
                $strategy2++;
            }
        }

        var_dump('Strategy 1: ' . $strategy1);
        var_dump('Strategy 2: ' . $strategy2);
    }
}
