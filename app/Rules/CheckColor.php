<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckColor implements Rule
{
    private $colors = ["blue", "green", "cyan", "red", "magenta", "yellow", "white"];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (in_array($value, $this->colors));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This color is not supported.';
    }
}
