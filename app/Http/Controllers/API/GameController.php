<?php

namespace App\Http\Controllers\API;

use App\Models\Game;
use App\Models\User;
use App\Rules\CheckColor;
use App\Rules\CheckOdd;
use App\Services\ComputerIntelligenceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Controller for Game API methods
 */
class GameController extends BaseController
{
    /**
     * API method which returns data about the requested game
     * @param $gameId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGame($gameId)
    {
        $game = new Game();
        $game = $game->getGame($gameId);
        if (is_null($game)) {
            return $this->sendError(
                'Игра с указанным ID не существует',
                [],
                404
            );
        }

        return $this->sendResponse($game->getGameData(), 'Текущее состояние игры.');
    }

    /**
     * API method which make step in the game
     * @param $gameId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeStep($gameId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'playerId' => ['required', 'integer', 'between:1,2'],
            'color' => ['required', new CheckColor()]
        ]);

        if ($validator->fails()) {
            return $this->sendError(
                'Неправильные параметры запроса',
                $validator->errors(),
                400
            );
        }

        $game = new Game();
        $game = $game->getGame($gameId);
        if (is_null($game)) {
            return $this->sendError(
                'Игра с указанным ID не существует',
                $validator->errors(),
                404
            );
        }

        $currentPlayer = User::query()
            ->where('game_uuid', '=', $game->uuid)
            ->where('player_id', '=', $request->input('playerId'))
            ->get()
            ->first();
        if ($request->input('playerId') == 1) {
            $anotherPlayer = User::query()
                ->where('game_uuid', '=', $game->uuid)
                ->where('player_id', '=', 2)
                ->get()
                ->first();
        } else {
            $anotherPlayer = User::query()
                ->where('game_uuid', '=', $game->uuid)
                ->where('player_id', '=', 1)
                ->get()
                ->first();
        }

        if ($request->input('playerId') != $game->current_player) {
            return $this->sendError(
                'Игрок с указанным номером не может сейчас ходить',
                $validator->errors(),
                403
            );
        }

        if ($game->winner != 0) {
            return $this->sendError(
                'Игрок с указанным номером не может сейчас ходить',
                $validator->errors(),
                403
            );
        }

        if ($request->input('color') == $anotherPlayer->color || $request->input('color') == $currentPlayer->color) {
            return $this->sendError(
                'Игрок с указанным номером не может выбрать указанный цвет',
                $validator->errors(),
                409
            );
        }

        $game->field->makeStep($request->input('playerId'), $request->input('color'));

        return $this->sendResponse(null, 'Ход успешно сделан.');
    }

    /**
     * API method which create game
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createGame(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'width' => ['required', 'integer', 'between:5,99', new CheckOdd()],
            'height' => ['required', 'integer', 'between:5,99', new CheckOdd()]
        ]);

        if($validator->fails()){
            return $this->sendError(
                'Ширина или высота не попадают в диапазоны минимального и максильного значения или четные',
                $validator->errors(),
                400
            );
        }

        $computerMode = 0;
        if (!is_null($request->input('computer_mode'))) {
            $computerMode = $request->input('computer_mode');
        }

        $game = new Game();
        $game = $game->createGame($request->input('width'), $request->input('height'), $computerMode);

        return $this->sendResponse($game->uuid, 'Поле создано.');
    }

    public function makeComputerStep($gameId, Request $request)
    {
        $computerService = new ComputerIntelligenceService(new Game());

        $validator = Validator::make($request->all(), [
            'computer_mode' => ['required', 'integer', 'between:1,3'],
        ]);

        if($validator->fails()){
            return $this->sendError(
                'Уровень сложности должен находиться в пределах от 1 до 3!',
                $validator->errors(),
                400
            );
        }

        $computerMode = (int) $request->input('computer_mode');
        $computerColor = '';

        $game = new Game();
        $game = $game->getGame($gameId);

        if (!is_null($game)) {
            if ($computerMode === 1) {
                $computerColor = $computerService->selectRandomColor($game);
            } elseif ($computerMode === 2) {
                $computerColor = $computerService->selectBestColor(2, $game);
            } elseif ($computerMode === 3) {
                $computerColor = $computerService->selectBestColorAdvanced(2, $game);
            }

            $request->merge(['color' => $computerColor, 'playerId' => 2]);
            $game->field->makeStep(2, $computerColor);
        }

        return $this->sendResponse(null, 'Ход компьютера успешно сделан.');
    }
}
