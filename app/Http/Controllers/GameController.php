<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class GameController
 * @package App\Http\Controllers
 */
class GameController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function connectGame(Request $request)
    {
        $params = $request->all();
        if (isset($params['connect_uuid'])) {
            return redirect()->route('game.start', ['gameId' => $params['connect_uuid']]);
        }
        return view('connect');
    }

    /**
     * @return Application|Factory|View
     */
    public function selectMode()
    {
        return view('select-mode');
    }

    /**
     * @return Application|Factory|View
     */
    public function difficultyLevel()
    {
        return view('difficulty-level');
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('create');
    }

    /**
     * @param Request $request
     * @param string  $gameId
     * @return Application|Factory|View
     */
    public function show(Request $request, string $gameId)
    {
        $game = Game::query()->select()->where('uuid', '=', $gameId)->first();

        return view('show', compact('game'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function store(Request $request): RedirectResponse
    {
        $params = $request->all();
        $game = new Game();
        $game = $game->createGame($params['game_width'], $params['game_height'], $params['computer_mode'] ?? 0);
        return redirect()->route('game.start', ['gameId' => $game->uuid]);
    }
}
