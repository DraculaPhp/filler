<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Player model
 */
class User extends Authenticatable
{
    use HasFactory;

    // remove unusable timestamps
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'color',
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class, 'game_uuid', 'uuid');
    }

    /**
     * Get data about player in the form that is needed for api
     * @return array
     */
    public function getUserData()
    {
        return [
            'color' => $this->color,
            'playerId' => $this->player_id
        ];
    }

    /**
     * Create player for game
     * @param $id
     * @param $name
     */
    public function createPlayer($name, $gameUUID, $playerID)
    {
        $user = new User();
        $user->name = $name;
        $user->player_id = $playerID;
        $user->game_uuid = $gameUUID;
        $user->save();
    }
}
