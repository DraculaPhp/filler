<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

/**
 * Model with the implementation of logic for actions taking place on the game field
 * @property int         id
 * @property int         width
 * @property int         height
 * @property string      game_uuid
 * @property Game        game
 * @property array<Cell> cells
 */
class Field extends Model
{
    // remove unusable timestamps
    public $timestamps = false;

    use HasFactory;

    /**
     * Connection with field cells (1 to N)
     * @return HasMany
     */
    public function cells(): HasMany
    {
        return $this->hasMany(Cell::class);
    }

    /**
     * Connection with game (1 to 1)
     * @return BelongsTo
     */
    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class, 'game_uuid', 'uuid');
    }

    /**
     * @param int $width
     * @param int $height
     * @param int $fieldId
     */
    public function fillField(int $width, int $height, int $fieldId): void
    {
        $cell = new Cell();
        $cellsData = [];

        // create cells and fill field
        for ($y = 0; $y <= $height - 1;  $y++) {
            if ($y % 2 === 0) {
                for ($x = $width * ($y / 2); $x <= (($width - 1) / 2) + $width * ($y / 2); $x++) {
                    $cellsData[] = $cell->createCellArray($x, 0, $fieldId);
                }
            } else {
                for ($x = ($width + 1) / 2 + $width * (($y - 1) / 2); $x < $width * (($y + 1) / 2); $x++) {
                    $cellsData[] = $cell->createCellArray($x, 0, $fieldId);
                }
            }
        }

        DB::table('cells')->insert($cellsData);
    }

    /**
     * Initialize game field, assign start cells for players
     *
     * @param int  $width  - from 5 to 99 only odd values
     * @param int  $height - from 5 to 99 only odd values
     * @param Game $game   - current game
     *
     * @return Field
     *
     * @throws Exception
     */
    public function createField(int $width, int $height, Game $game): Field
    {
        // initialize field
        $field = new Field();
        $field->width = $width;
        $field->height = $height;
        $field->game_uuid = $game->uuid;
        $field->save();
        $cell = new Cell();

        $this->fillField($width, $height, $field->id);

        $player1StartCell = $this->getStartCellForFirstPlayer($field->id, $field->width);
        $player2StartCell = $this->getStartCellForSecondPlayer($field->id, $field->width);

        if ($player1StartCell->color === $player2StartCell->color) {
            $allColors = $cell->getAllColors();
            foreach ($allColors as $color) {
                if ($player1StartCell->color !== $color) {
                    $player1StartCell->update(['color' => $color]);
                    break;
                }
            }
        }

        $this->assignStartCellToPlayer($player1StartCell, 1, $game->uuid);
        $this->assignStartCellToPlayer($player2StartCell, 2, $game->uuid);

        // if next to the first cell of the player there are cells of the same color,
        // they are automatically assigned to the player before first step
        $this->assignAdditionalStartCells($player1StartCell, $player2StartCell);

        return $field;
    }

    /**
     * get first cell for first player
     *
     * @param int $fieldId
     * @param int $width
     *
     * @return mixed
     */
    public function getStartCellForFirstPlayer(int $fieldId, int $width)
    {
        $count = Cell::query()
            ->where('field_id', '=', $fieldId)
            ->count('*');
        $cellId = $count - ($width + 1) / 2;

        $cell = Cell::query()
            ->where('number', '=', $cellId)
            ->where('field_id', '=', $fieldId);

        return $cell->first();
    }

    /**
     * get first cell for second player
     *
     * @param int $fieldId
     * @param int $width
     *
     * @return mixed
     */
    public function getStartCellForSecondPlayer(int $fieldId, int $width)
    {
        $cell = Cell::query()
            ->where('number', '=', ($width - 1) / 2)
            ->where('field_id', '=', $fieldId);

        return $cell->first();
    }

    /**
     * Assign start cell and start color for players
     * for the first player this is cell in the lower left corner
     * for the second player this is cell in the higher right corner
     *
     * @param Cell   $cell
     * @param int    $playerId
     * @param string $gameId
     */
    public function assignStartCellToPlayer(Cell $cell, int $playerId, string $gameId): void
    {
        $player = User::query()
            ->where('player_id', '=', $playerId)
            ->where('game_uuid', '=', $gameId);
        $player->update(['color' => $cell->color]);
        $cell->update(['player_id' => $playerId]);
    }

    /**
     * Assign additional start cells, if first start player
     * cell has neighbors with the same color
     *
     * @param Cell $cell1 - start cell fir first player
     * @param Cell $cell2 - start cell for second player
     */
    public function assignAdditionalStartCells(Cell $cell1, Cell $cell2): void
    {
        $this->assignCellsToPlayerRecursive($cell1, 1);
        $this->assignCellsToPlayerRecursive($cell2, 2);
    }

    /**
     * Get all current user cells at current field
     *
     * @param int $playerId
     * @param int $fieldId
     *
     * @return array
     */
    public function getCellsByPlayerId(int $playerId, int $fieldId): array
    {
        $cells = Cell::query()
            ->where('player_id', '=', $playerId)
            ->where('field_id', '=', $fieldId)
            ->get();
        $cellsData = [];
        foreach ($cells as $cell) {
            $cellsData[$cell->number] = $cell;
        }
        return $cellsData;
    }

    /**
     * Return array with neighbors (from 1 to 4 depending on the location of the cell)
     *
     * @param int $number   - the number of the cell whose neighbors you need to find out
     * @param int $width    - width of current game field
     * @param int $field_id - field id
     *
     * @return array
     */
    public function getNeighboringCells(int $number, int $width, int $field_id): array
    {
        $cells = [];
        $cases = [];
        if (($number - ($width - 1) / 2) % $width !== 0) {
            $cases[] = $number + ($width + 1) / 2;
            $cases[] = $number - ($width - 1) / 2;
        }
        if ($number % $width !== 0) {
            $cases[] = $number + ($width - 1) / 2;
            $cases[] = $number - ($width + 1) / 2;
        }
        foreach ($cases as $case) {
            $cell = Cell::query()
                ->where('number', '=', $case)
                ->where('field_id', '=', $field_id)
                ->first();
            if (!is_null($cell)) {
                $cells[$cell->number] = $cell;
            }
        }

        return $cells;
    }

    /**
     * Makes a step in the current game
     *
     * @param int    $playerId  - id of the player who makes the step
     * @param string $nextColor - next color for players cells
     */
    public function makeStep(int $playerId, string $nextColor): void
    {
        $user = User::query()
            ->where('player_id', '=', $playerId)
            ->where('game_uuid', '=', $this->game_uuid)
            ->first();

        $currentUserCells = $this->getCellsByPlayerId($playerId, $this->id);

        $this->changeCellsColor($currentUserCells, $nextColor, $this->id);

        $currentUserCells = $this->getCellsByPlayerId($playerId, $this->id);

        foreach ($currentUserCells as $cell) {
            $this->assignCellsToPlayerRecursive($cell, $playerId);
        }

        $user->update(['color' => $nextColor]);

        $this->game->changeCurrentPlayer();

        if ($this->checkWinner($this->getCellsByPlayerId($playerId, $this->id), $this->id)) {
            $this->game->setWinner($this->game, $playerId);
        }
    }

    /**
     * Implementation of the logic of recursive capturing cells by the player
     *
     * @param Cell $cell - player's cell
     * @param int  $playerId - current player id
     */
    public function assignCellsToPlayerRecursive(Cell $cell, int $playerId): void
    {
        ini_set('xdebug.max_nesting_level', 2000);

        $neighboringCells = $this->getNeighboringCells($cell->number, $cell->field->width, $cell->field->id);
        $neighboringCellsNextColor = [];
        foreach ($neighboringCells as $neighboringCell) {
            if ($neighboringCell['color'] == $cell->color) {
                $neighboringCellsNextColor[] = $neighboringCell;
            }
        }

        if (empty($neighboringCellsNextColor)) {
            return;
        }

        foreach ($neighboringCellsNextColor as $neighboringCell) {
            if ($neighboringCell['player_id'] != $playerId) {
                $neighboringCell->update(['player_id' => $playerId]);
                $this->assignCellsToPlayerRecursive($neighboringCell, $playerId);
            }
        }
    }

    /**
     * Return data about current game field
     * in the form that is needed for api
     *
     * @return array
     */
    public function getFieldData(): array
    {
        $cellsData = [];
        $cells = $this->cells;
        foreach ($cells as $cell) {
            $cellsData[$cell->number] = [
                'color' => $cell->color,
                'playerId' => $cell->player_id
            ];
        }
        return [
            'width' => $this->width,
            'height' => $this->height,
            'cells' => $cellsData
        ];
    }

    /**
     * Repaint cells in the next color
     * @param $cells - cells to be repainted
     * @param $nextColor - next color for players cells
     */
    public function changeCellsColor($cells, $nextColor, $fieldId): void
    {
        DB::table('cells')
            ->where('field_id', '=', $fieldId)
            ->whereIn('number', array_keys($cells))
            ->update(['color' => $nextColor]);
    }

    /**
     * Determines if the current user has enough cells to win (>=50% of all)
     *
     * @param $playerCells - current player cells
     * @param $fieldId     - current game field id
     *
     * @return bool
     */
    public function checkWinner($playerCells, $fieldId): bool
    {
        $countCells = count(Cell::all()->where('field_id', '=', $fieldId));

        return count($playerCells) >= $countCells / 2;
    }
}
