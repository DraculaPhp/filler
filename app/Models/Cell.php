<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Model of field cell
 *
 * @property string color
 * @property int    number
 * @property int    player_id
 * @property int    field_id
 * @property Field  field
 */
class Cell extends Model
{
    // remove unusable timestamps
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'color',
        'player_id'
    ];

    private $colors = ["blue", "green", "cyan", "red", "magenta", "yellow", "white"];

    public function getAllColors(): array
    {
        return $this->colors;
    }

    use HasFactory;

    /**
     * Define random color for game cell
     *
     * @return string
     * @throws Exception
     */
    public function getRandomColor(): string
    {
        $color = '';
        $index = random_int(0, 6);
        foreach ($this->colors as $key => $value) {
            if ($index === $key) {
                $color = $value;
            }
        }
        return $color;
    }

    /**
     * Create cell for game field
     *
     * @param int   $number   - cell number on the playing field
     * @param int   $playerId - the number of the player who owns the cell
     * @param Field $field    - the id of the field which owns the cell
     *
     * @return Cell
     * @throws Exception
     */
    public function createCell(int $number, int $playerId, Field $field): Cell
    {
        $cell = new Cell();
        $cell->color = $cell->getRandomColor();
        $cell->number = $number;
        $cell->player_id = $playerId;
        $cell->field_id = $field->id;
        $cell->save();

        return $cell;
    }

    public function createCellArray(int $number, int $playerId, int $fieldId)
    {
        return [
            'number' => $number,
            'player_id' => $playerId,
            'color' => $this->getRandomColor(),
            'field_id' => $fieldId
        ];
    }

    /**
     * Connection with game field
     * @return BelongsTo
     */
    public function field(): BelongsTo
    {
        return $this->belongsTo(Field::class);
    }
}
