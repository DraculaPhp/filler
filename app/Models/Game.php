<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;

/**
 * Model with implementation of game logic
 *
 * @property string uuid
 * @property int winner
 * @property int current_player
 * @property int computer_mode
 * @property Field field
 */
class Game extends Model
{
    // remove unusable timestamps
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'uuid',
        'winner',
        'current_player'
    ];

    use HasFactory;

    /**
     * Connection with game field (1 to 1)
     * @return HasOne
     */
    public function field(): HasOne
    {
        return $this->hasOne(Field::class, 'game_uuid', 'uuid');
    }

    /**
     * @return HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'game_uuid', 'uuid');
    }

    /**
     * Create and initialize game and start field
     *
     * @param int $width  - from 5 to 99 only odd values
     * @param int $height - from 5 to 99 only odd values
     *
     * @return Game
     * @throws Exception
     */
    public function createGame(int $width, int $height, int $computerMode = 0): Game
    {
        $game = new Game();
        $game->uuid = Str::uuid();
        $game->current_player = 1;
        $game->winner = 0;
        $game->computer_mode = $computerMode;
        $game->save();

        $this->createPlayersForCurrentGame($game);

        $field = new Field();
        $field->createField($width, $height, $game);

        return $game;
    }

    /**
     * Create users for game if they don`t exist
     */
    public function createPlayersForCurrentGame(Game $game): void
    {
        $user = new User();
        $user->createPlayer('First Player', $game->uuid, 1);
        $user->createPlayer('Second Player', $game->uuid, 2);
    }

    /**
     * Set winner for current game
     * @param $game - current game
     * @param $playerId - winner
     */
    public function setWinner($game, $playerId): void
    {
        $game->update(['winner' => $playerId]);
    }

    /**
     * Changes the user's turn queue
     */
    public function changeCurrentPlayer(): void
    {
        $currentPlayer = $this->current_player;
        if ($currentPlayer === 1) {
            $this->update(['current_player' => 2]);
        } else {
            $this->update(['current_player' => 1]);
        }
    }

    /**
     * Get game example by uuid
     *
     * @param string $uuid - unique game key
     *
     * @return Builder|Model|object|null
     */
    public function getGame(string $uuid): Game
    {
        return self::query()
            ->where('uuid', '=', $uuid)
            ->first();
    }

    /**
     * Return all information about game, current game field state, cells
     * and also players
     *
     * @return array
     */
    public function getGameData(): array
    {
        $gameData = [];
        $gameData['uuid'] = $this->uuid;
        $gameData['field'] = $this->field->getFieldData();
        $gameData['players'] = [
            User::query()
                ->where('game_uuid', '=', $this->uuid)
                ->where('player_id', '=', 1)
                ->get()
                ->first()
                ->getUserData(),
            User::query()
                ->where('game_uuid', '=', $this->uuid)
                ->where('player_id', '=', 2)
                ->get()
                ->first()
                ->getUserData()
        ];
        $gameData["currentPlayerId"] = $this->current_player;
        $gameData["winner"] = $this->winner;
        $gameData['computerMode'] = $this->computer_mode;
        return $gameData;
    }

    public function getAvailableColors()
    {
        $gamePlayers = $this->getGameData()['players'];
        $bookedColors = [];
        $allColors = ["blue", "green", "cyan", "red", "magenta", "yellow", "white"];
        foreach ($gamePlayers as $gamePlayer) {
            $bookedColors[] = $gamePlayer['color'];
        }
        foreach ($allColors as $key => $value) {
            if (in_array($value, $bookedColors)) {
                unset($allColors[$key]);
            }
        }
        return $allColors;
    }
}
