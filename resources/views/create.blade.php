@extends('layouts.app')

@section('content')
    <div id="create_game_window" tabindex="1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Новая игра</h5>
                </div>
                <div class="modal-body">
                    <form
                        @if (isset($_REQUEST['computer_mode']))
                            action="{{ route('game.store', $_REQUEST['computer_mode']) }}"
                        @else
                            action="{{ route('game.store') }}"
                        @endif method="POST" class="mt-3">
                        @csrf
                        @if (isset($_REQUEST['computer_mode']))
                            <input class="form-control" id="computer_mode" name="computer_mode" value="{{$_REQUEST['computer_mode']}}" hidden>
                        @endif
                        <div class="form-floating mb-3">
                            <input class="form-control" id="game_width" name="game_width" placeholder="5-99 (Нечетное число)">
                            <label for="game_width">Ширина</label>
                        </div>
                        <div class="form-floating">
                            <input class="form-control" id="game_height" name="game_height" placeholder="5-99 (Нечетное число)">
                            <label for="game_height">Высота</label>
                        </div>
                        <button type="submit" class="btn btn-info mt-3">Создать игру</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-info" href="{{ route('game.difficulty-level') }}">Назад</a>
                </div>
            </div>
        </div>
    </div>
@endsection
