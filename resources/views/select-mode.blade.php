@extends('layouts.app')

@section('content')
    <div id="select_game_mode" tabindex="1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">7 Цветов (Filler)</h5>
                </div>
                <div class="modal-body">
                    <h4>Select game mode:</h4>
                    <a class="btn btn-info" href="{{ route('game.difficulty-level') }}">Одиночная игра</a>
                    <a class="btn btn-info" href="{{ route('game.create') }}">Играть вдвоём</a>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-info" href="{{ route('game.index') }}">Назад</a>
                </div>
            </div>
        </div>
    </div>
@endsection
