@extends('layouts.app')

@section('content')
    <div id="select_difficulty_level" tabindex="1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">7 Цветов (Filler)</h5>
                </div>
                <div class="modal-body">
                    <h4>Select difficulty level:</h4>
                    <form method="GET" action="{{ route('game.create') }}" class="mt-3">
                        <div class="form-check mt-3">
                            <input class="form-check-input" type="radio" id="easy_mode" name="computer_mode" value="1" checked="checked">
                            <label class="form-check-label" for="easy_mode">
                                Easy
                            </label>
                        </div>
                        <div class="form-check mt-3">
                            <input class="form-check-input" type="radio" id="medium_mode" name="computer_mode" value="2">
                            <label class="form-check-label" for="medium_mode">
                                Medium
                            </label>
                        </div>
                        <div class="form-check mt-3">
                            <input class="form-check-input" type="radio" id="hard_mode" name="computer_mode" value="3">
                            <label class="form-check-label" for="hard_mode">
                                Hard
                            </label>
                        </div>
                        <button type="submit" class="btn btn-info mt-3">Создать игру</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-info" href="{{ route('game.select-mode') }}">Назад</a>
                    <a class="btn btn-info" href="{{ route('game.create') }}">Создать игру</a>
                </div>
            </div>
        </div>
    </div>
@endsection
