@extends('layouts.app')

@section('content')
    <div id="welcome_window" tabindex="1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">7 Цветов (Filler)</h5>
                </div>
                <div class="modal-body">
                    <a class="btn btn-info" href="{{ route('game.connect') }}">Присоединиться</a>
                    <a class="btn btn-primary" href="{{ route('game.select-mode') }}">Создать игру</a>
                </div>
            </div>
        </div>
    </div>
@endsection
