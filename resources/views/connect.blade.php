@extends('layouts.app')

@section('content')
    <div id="connect_to_game_window" tabindex="1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Присоедениться к игре</h5>
                </div>
                <div class="modal-body">
                    <form action="{{ route('game.connect') }}" method="POST" class="mt-3">
                        @csrf
                        <div class="form-floating mb-3">
                            <input class="form-control" id="connect_uuid" name="connect_uuid" placeholder="5-99 (Нечетное число)">
                            <label for="connect_uuid">UUID игры</label>
                        </div>
                        <button type="submit" class="btn btn-info mt-3">Присоединиться</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-info" href="{{ route('game.index') }}">Назад</a>
                </div>
            </div>
        </div>
    </div>
@endsection
