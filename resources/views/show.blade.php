@extends('layouts.app')

@section('content')
    <div id="game" class="gold-text">
        <h2 class="text-center"><span id="gameUUID">{{$game->uuid}}</span></h2>
        <a class="btn btn-info" href="{{ route('game.index') }}">Назад</a>
        <h2 class="text-center"><span id="gameStatus">Пожалуйста, подождите. Игра загружается!</span></h2>
        <h3 class="text-center">Первый игрок: <span id="player1">0%</span></h3>
        <h3 class="text-center">Второй игрок: <span id="player2">0%</span></h3>
        <div id="color_buttons" class="d-grid gap-2 d-md-flex justify-content-md-center"></div>
        <div class="d-md-flex justify-content-md-center mt-3">
            <canvas id="canvas"></canvas>
        </div>
    </div>
    <script>
        const canvas = document.getElementById("canvas");
        const ctx = canvas.getContext("2d");

        const gameUUID = document.getElementById("gameUUID").innerText;
        console.log(gameUUID);
        const p1percent = document.getElementById("player1");
        const p2percent = document.getElementById("player2");
        const color_buttons = document.getElementById("color_buttons");
        const gameStatus = document.getElementById("gameStatus");
        let gameSession;

        class GameSession {
            uuid;
            data;
            makeStep(color) {
                const xmlHttp = new XMLHttpRequest();
                this.refreshField();
                const response = this.data;
                const currentPlayerId = response.data.currentPlayerId;
                xmlHttp.open( "PUT", `/api/game/${this.uuid}?playerId=${currentPlayerId}&color=${color}`, false );
                xmlHttp.send( null );
                switch (xmlHttp.status) {
                    case 201:
                        break;
                    case 400:
                        alert("Неверные данные");
                        break;
                    case 403:
                        alert("Сейчас не ваш ход!");
                        break;
                    case 404:
                        alert("Игра не найдена!");
                        break;
                    case 409:
                        alert("Цвет уже занят одним из игроков!");
                        break;
                    default:
                        alert(`Упс! Что-то пошло не так! (${xmlHttp.status})`);
                        break;
                }
            }
            makeComputerStep() {
                const xmlHttp = new XMLHttpRequest();
                this.refreshField();
                const response = this.data;
                const computerMode = response.data.computerMode;
                xmlHttp.open("PUT", `/api/game/single/${this.uuid}?computer_mode=${computerMode}`, false);
                xmlHttp.send( null );
                switch (xmlHttp.status) {
                    case 201:
                        break;
                    case 400:
                        alert("Неверные данные");
                        break;
                    case 403:
                        alert("Сейчас не ваш ход!");
                        break;
                    case 404:
                        alert("Игра не найдена!");
                        break;
                    case 409:
                        alert("Цвет уже занят одним из игроков!");
                        break;
                    default:
                        alert(`Упс! Что-то пошло не так! (${xmlHttp.status})`);
                        break;
                }
            }
            refreshField() {
                const xmlHttp = new XMLHttpRequest();
                xmlHttp.open( "GET", `/api/game/${this.uuid}`, false );
                xmlHttp.send( null );
                const data = xmlHttp.responseText;
                this.data = JSON.parse(data);
            }
            constructor(uuid) {
                this.uuid = uuid;
                this.refreshField();
            }
        }

        function connectGame(uuid) {
            const xtr = new XMLHttpRequest();
            xtr.open( "GET", `/api/game/${uuid}`, false );
            xtr.send( null );
            switch (xtr.status) {
                case 201:
                    gameSession = new GameSession(uuid);
                    break;
                case 400:
                    alert("Неверные данные");
                    break;
                case 404:
                    alert("Игра не найдена!");
                    break;
                default:
                    alert(`Произошла неизвестная ошибка (${xtr.status})`);
                    break;
            }
        }

        connectGame(gameUUID);

        class Color {
            constructor(color, name) {
                this.color = color;
                this.name = name;
            }
        }

        const colors = [
            new Color("rgb(0,0,200)", "blue"),
            new Color("rgb(0,200,0)", "green"),
            new Color("rgb(0,200,200)", "cyan"),
            new Color("rgb(200,0,0)", "red"),
            new Color("rgb(200,0,200)", "magenta"),
            new Color("rgb(200,200,0)", "yellow"),
            new Color("rgb(255, 255, 255)", "white")
        ];

        function drawDiamondCell(x, y, color, player=0, size=80) {
            color = colors.find(element => {
                if (element.name === color) {
                    return true;
                }
            });
            ctx.fillStyle = color.color;
            ctx.beginPath();
            ctx.moveTo(x * size + size / 2, y * size);
            ctx.lineTo(x * size + size, y * size + size / 2);
            ctx.lineTo(x * size + size / 2, y * size + size);
            ctx.lineTo(x * size, y * size + size / 2);
            ctx.fill();
            ctx.fillStyle = 'black';
            if (player !== 0) {
                ctx.fillText(player.toString(), x * size + 10, y * size + size / 2);
            }
        }

        function createButtons(colors, container) {
            for (const color of colors) {
                let button = document.createElement("button");
                button.classList.add("btn");
                button.classList.add("btn-primary");
                button.classList.add("black-text");
                button.style = `background-color: ${color.color}`;
                button.innerText = color.name;
                button.onclick = function() {
                    gameSession.makeStep(color.name);
                };
                container.append(button);
            }
        }

        createButtons(colors, color_buttons);

        function refreshScreen() {
            if (gameSession) {
                let number = 0;
                let column;
                gameSession.refreshField();
                const responce = gameSession.data;
                const {currentPlayerId, winner, computerMode} = responce.data;
                const {width, height, cells} = responce.data.field;

                if (currentPlayerId === 2 && computerMode !== 0 && winner === 0) {
                    gameSession.makeComputerStep();
                }

                if (winner === 0) {
                    gameStatus.innerText = `Ходит игрок ${currentPlayerId}`;
                } else {
                    gameStatus.innerText = `Победил игрок ${winner}`;
                }

                canvas.width = (width + 1) / 2 * 80;
                canvas.height = (height + 1) / 2 * 80;
                const countCells = cells.length;
                let p1cells = 0;
                let p2cells = 0;
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                for (const cell of cells) {
                    if (cell.playerId === 1) {
                        p1cells++;
                    }
                    if (cell.playerId === 2) {
                        p2cells++;
                    }
                }
                let y = 0;

                for (column = 0; column <= height - 1;  column++) {
                    if (column % 2 === 0) {
                        let x = 0;
                        for (number = width * (column / 2); number <= ((width - 1) / 2) + width * (column / 2); number++) {
                            drawDiamondCell(x, y, cells[number].color, cells[number].playerId);
                            x++;
                        }
                    } else {
                        let x = 0.5;
                        for (number = (width + 1) / 2 + width * ((column - 1) / 2); number < width * ((column + 1) / 2); number++) {
                            drawDiamondCell(x, y, cells[number].color, cells[number].playerId);
                            x++;
                        }
                    }
                    y += 0.5;
                }
                p1percent.innerText = `${Math.floor((p1cells * 100) / countCells)}%`;
                p2percent.innerText = `${Math.floor((p2cells * 100) / countCells)}%`;
            }
        }
        setInterval(() => {
            refreshScreen()
        }, 3000);
    </script>
@endsection
