<?php

namespace Tests\Feature;

use App\Models\Game;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Test class for Game methods
 */
class GameTest extends TestCase
{
    Use RefreshDatabase;

    /**
     * Test foe create game
     *
     * @return void
     */
    public function testGame()
    {
        $game = new Game();
        $game = $game->createGame(5,5);
        $gameById = $game->getGame($game->uuid);

        $this->assertEquals($game->getAttributes(), $gameById->getAttributes());
        $this->assertEquals(0, $game->winner);
        $this->assertEquals(1, $game->current_player);
        $this->assertTrue(!is_null($game->field));

        $this->assertEquals(2, count($game->users));
        $this->assertNotEquals($game->users[0]->color, $game->users[1]->color);

        $this->assertEquals(5, $game->field->width);
        $this->assertEquals(5, $game->field->height);

        $game->changeCurrentPlayer();
        $this->assertEquals(2, $game->current_player);

        $game->setWinner($game, 1);
        $this->assertEquals(1, $game->winner);
    }
}
