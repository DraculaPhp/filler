<?php

namespace Tests\Feature;

use App\Models\Cell;
use App\Models\Game;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Test class foe Cell methods
 */
class CellTest extends TestCase
{
    Use RefreshDatabase;

    /**
     * Test for create cell
     */
    public function testCreateCell()
    {
        $game = new Game();
        $game = $game->createGame(5,5);

        $cell = new Cell();
        $newCell = $cell->createCell(1, 0, $game->field);
        $colors = $cell->getAllColors();

        $this->assertEquals(1, $newCell->number);
        $this->assertEquals(0, $newCell->player_id);
        $this->assertEquals($game->field->id, $newCell->field_id);
        $this->assertTrue(in_array($newCell->color, $colors));
    }
}
