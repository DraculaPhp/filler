<?php

namespace Tests\Feature\API;

use App\Models\Cell;
use App\Models\Game;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

/**
 * Test for Game API methods
 */
class GameTest extends TestCase
{
    Use RefreshDatabase;

    /**
     * Test for POST API method (success result)
     */
    public function testCreateGameWithRightParameters()
    {
        $width = 5;
        $height = 5;
        $response = $this->post("/api/game/?width=$width&height=$height");
        $response->assertStatus(201);
    }

    /**
     * Test for POST API method (Wrong result)
     */
    public function testCreateGameWithWrongParameters()
    {
        $width = 3;
        $height = 5;
        $response = $this->post("/api/game/?width=$width&height=$height");
        $response->assertStatus(400);

        $width = 5;
        $height = 3;
        $response = $this->post("/api/game/?width=$width&height=$height");
        $response->assertStatus(400);

        $width = 6;
        $height = 5;
        $response = $this->post("/api/game/?width=$width&height=$height");
        $response->assertStatus(400);

        $width = 5;
        $height = 6;
        $response = $this->post("/api/game/?width=$width&height=$height");
        $response->assertStatus(400);

        $width = 101;
        $height = 5;
        $response = $this->post("/api/game/?width=$width&height=$height");
        $response->assertStatus(400);

        $width = 5;
        $height = 101;
        $response = $this->post("/api/game/?width=$width&height=$height");
        $response->assertStatus(400);
    }

    /**
     * Test for GET API method
     */
    public function testGetGame()
    {
        $game = new Game();
        $game = $game->createGame(5,5);

        $response = $this->get("/api/game/$game->uuid");
        $response->assertStatus(201);

        $data = json_decode($response->getContent(), true)['data'];
        $this->assertTrue(!is_null($data));

        $testUUID = Str::uuid();
        $response = $this->get("/api/game/$testUUID");
        $response->assertStatus(404);
    }

    /**
     * Test for PUT API method
     */
    public function testMakeStep()
    {
        $game = new Game();
        $cell = new Cell();
        $game = $game->createGame(5,5);
        $game = $game->getGame($game->uuid);

        $testUUID = Str::uuid();
        $gamePlayers = $game->users;
        $bookedColors = [];
        $allColors = $cell->getAllColors();
        $testColor = '';

        foreach ($gamePlayers as $gamePlayer) {
            $bookedColors[] = $gamePlayer->color;
        }

        foreach ($allColors as $color) {
            if (!in_array($color, $bookedColors)) {
                $testColor = $color;
                break;
            }
        }

        $anotherPlayerId = 1;
        if ($game->current_player == 1) {
            $anotherPlayerId = 2;
        }

        $response = $this->put("/api/game/$testUUID?playerId=$game->current_player&color=$testColor");
        $response->assertStatus(404);

        $response = $this->put("/api/game/$testUUID?playerId=0&color=$testColor");
        $response->assertStatus(400);

        $response = $this->put("/api/game/$testUUID?playerId=$game->current_player&color=null");
        $response->assertStatus(400);

        $response = $this->put("/api/game/$game->uuid?playerId=$game->current_player&color=$bookedColors[0]");
        $response->assertStatus(409);

        $response = $this->put("/api/game/$game->uuid?playerId=$game->current_player&color=$bookedColors[1]");
        $response->assertStatus(409);

        if ($game->winner != 0) {
            $response = $this->put("/api/game/$game->uuid?playerId=$game->current_player&color=$testColor");
            $response->assertStatus(403);
        }

        $response = $this->put("/api/game/$game->uuid?playerId=$anotherPlayerId&color=$testColor");
        $response->assertStatus(403);

        $response = $this->put("/api/game/$game->uuid?playerId=$game->current_player&color=$testColor");
        $response->assertStatus(201);
    }
}
