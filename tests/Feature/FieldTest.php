<?php

namespace Tests\Feature;

use App\Models\Game;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Test class for Field methods
 */
class FieldTest extends TestCase
{
    Use RefreshDatabase;

    /**
     * Test for create field
     *
     * @return void
     */
    public function testCreateField()
    {
        $game = new Game();
        $game = $game->createGame(5,5);

        $this->assertEquals($game->uuid, $game->field->game_uuid);
        $this->assertCount(13, $game->field->cells);
        $this->assertEquals(10, $game->field->getStartCellForFirstPlayer($game->field->id, 5)->number);
        $this->assertEquals(2, $game->field->getStartCellForSecondPlayer($game->field->id, 5)->number);
    }

    /**
     * Test for getNeighboringCells method
     */
    public function testGetNeighboringCells()
    {
        $game = new Game();
        $game = $game->createGame(5,5);

        $this->assertCount(1, $game->field->getNeighboringCells(2, 5, $game->field->id));
        $this->assertEquals(4, $game->field->getNeighboringCells(2, 5, $game->field->id)[4]->number);

        $this->assertCount(2, $game->field->getNeighboringCells(5, 5, $game->field->id));
        $this->assertEquals(8, $game->field->getNeighboringCells(5, 5, $game->field->id)[8]->number);
        $this->assertEquals(3, $game->field->getNeighboringCells(5, 5, $game->field->id)[3]->number);

        $this->assertCount(4, $game->field->getNeighboringCells(3, 5, $game->field->id));
        $this->assertEquals(0, $game->field->getNeighboringCells(3, 5, $game->field->id)[0]->number);
        $this->assertEquals(1, $game->field->getNeighboringCells(3, 5, $game->field->id)[1]->number);
        $this->assertEquals(5, $game->field->getNeighboringCells(3, 5, $game->field->id)[5]->number);
        $this->assertEquals(6, $game->field->getNeighboringCells(3, 5, $game->field->id)[6]->number);
    }
}
